@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 d-none d-md-block">
			<div class="card" data-aos="fade-up" data-aos-delay="300">
				<div class="card-body">
					<x-sidebar-menu activeMenu="material" />
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card full-height" data-aos="zoom-in">
				<div class="my-3 m-md-4 p-3">
					<h2 class="mb-3 text-center d-none" data-aos="fade-up" data-aos-delay="100">Materi Rapat Anggota
					</h2>

					@if ($materials->count() == 0)
					<h5 class="mt-5 mt-md-3 text-muted text-center">
						Tidak ada materi yang tersedia
					</h5>
					@else
					<div class="m-3 text-muted text-center">Halaman</div>
					@endif

					<ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
						@foreach ($materials as $item)
						<li class="nav-item" role="presentation">
							<a class="nav-link {{ $loop->first ? 'active' : '' }}" id="pills-{{ $item->id }}-tab"
							   data-toggle="pill" href="#pills-{{ $item->id }}" role="tab"
							   aria-selected="true">{{ $loop->iteration }}</a>
						</li>
						@endforeach
					</ul>

					<div class="tab-content mt-5" id="pills-tabContent">
						@foreach ($materials as $item)
						<div class="tab-pane fade show {{ $loop->first ? 'active' : '' }}" id="pills-{{ $item->id }}"
							 role="tabpanel">

							<h4 class="font-weight-bold text-center">{{ $item->title }}</h4>

							<div class="my-4">
								{!! $item->body !!}
							</div>

							@if ($item->document_path)
							<div id="adobe-dc-view-{{ $item->id }}" style="height: 100vh"></div>
							<script src="https://documentservices.adobe.com/view-sdk/viewer.js"></script>
							<script type="text/javascript">
								document.addEventListener("adobe_dc_view_sdk.ready", function()
									{
									var adobeDCView = new AdobeDC.View({clientId: "{{ config('services.adobe.key') }}", divId: "adobe-dc-view-{{ $item->id }}"});
										adobeDCView.previewFile(
										{
											content: {location: {url: "{{ Storage::url($item->document_path) }}"}},
											metaData: {fileName: "{{ $item->document_origin_name }}"}
										},{
											defaultViewMode: "FIT_WIDTH", 
											showAnnotationTools: false, 
											showLeftHandPanel: false,
											dockPageControls: false, 
											showDownloadPDF: false, 
											showPrintPDF: false
										});
									});
							</script>
							@endif

							@if ($loop->last)
							<div class="row mt-3">
								<div class="col-md-12">
									<div class="d-flex justify-content-center my-3">
										<a href="{{ route('feedback') }}" class="btn btn-danger px-4">Lanjut</a>
									</div>
								</div>
							</div>
							@endif

						</div>
						@endforeach
					</div>
				</div>
			</div>

		</div>
	</div>
	@endsection

	@push('addon-styles')
	<style>
		.nav-pills .nav-link.active,
		.nav-pills .show>.nav-link {
			background-color: #2E3191;
			font-weight: bold;
			border-radius: 2rem;

		}

		.nav-pills .nav-link {
			color: #2E3191;
			font-weight: bold;
		}
	</style>
	@endpush