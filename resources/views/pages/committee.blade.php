@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 d-none d-md-block">
			<div class="card" data-aos="fade-up" data-aos-delay="300">
				<div class="card-body">
					<x-sidebar-menu activeMenu="committee" />
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card full-height" data-aos="zoom-in">
				<div class="my-3 m-md-4 p-3">
					<h2 class="text-center" data-aos="fade-up" data-aos-delay="100">Pengelola Koperasi</h2>
					<div class="row justify-content-center">
						<div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
							<div class="committee-board">
								<div class="">
									<img src="{{ asset('/storage/images/grha-surya.jpg')}}" alt="grha-surya"
										class="mx-auto mb-3 committee-image">
								</div>
								<div class="committee-name">Sonia</div>
								<div class="committee-title">Ketua Koperasi</div>
							</div>
						</div>
					</div>

					<h2 class="text-center mt-4" data-aos="fade-up" data-aos-delay="100">Pengurus Koperasi</h2>
					<div class="row justify-content-center">
						<div class="col-sm-12 col-md-4" data-aos="fade-up" data-aos-delay="100">
							<div class="committee-board">
								<div class="">
									<img src="{{ asset('/storage/images/grha-surya.jpg')}}" alt="grha-surya"
										class="mx-auto mb-3 committee-image">
								</div>
								<div class="committee-name">Sonia</div>
								<div class="committee-title">Ketua Koperasi</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-4" data-aos="fade-up" data-aos-delay="200">
							<div class="committee-board">
								<div class="">
									<img src="{{ asset('/storage/images/grha-surya.jpg')}}" alt="grha-surya"
										class="mx-auto mb-3 committee-image">
								</div>
								<div class="committee-name">Sonia</div>
								<div class="committee-title">Bendahara Koperasi</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-4" data-aos="fade-up" data-aos-delay="300">
							<div class="committee-board">
								<div class="">
									<img src="{{ asset('/storage/images/grha-surya.jpg')}}" alt="grha-surya"
										class="mx-auto mb-3 committee-image">
								</div>
								<div class="committee-name">Sonia</div>
								<div class="committee-title">Sekretaris Koperasi</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection