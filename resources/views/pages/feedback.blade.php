@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 d-none d-md-block">
			<div class="card" data-aos="fade-up" data-aos-delay="300">
				<div class="card-body">
					<x-sidebar-menu activeMenu="feedback" />
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card full-height" data-aos="zoom-in">
				<div class="my-3 m-md-4 p-3">
					{{-- Permission Start --}}
					@if (Auth::user()->hasPermissionTo('submit feedback'))

					{{-- Has submitted feedback START --}}
					@if ($feedback > 0)
					<div class="text-center d-flex align-items-center justify-content-center" style="height: 70vh">
						<div>
							<h1 class="display-6">Pendapat anda telah kami terima</h1>
							<h5 class="mt-5 mt-md-3 text-muted">Terima kasih telah berpartisipasi pada rapat anggota ini
							</h5>

							<div class="row d-md-none">
								<div class="col-md-12">
									<div class="d-flex justify-content-center my-3">
										<a class="btn btn-danger px-4" href="{{ route('logout') }}" onclick="event.preventDefault();
												document.getElementById('logout-form').submit();">
											{{ __('Logout') }}
										</a>
									</div>
								</div>
							</div>

						</div>
					</div>

					{{-- Has submitted feedback ELSE --}}
					@else
					<h2 class="mb-3" data-aos="fade-up" data-aos-delay="100">Pendapat Anggota</h2>

					@if ($errors->any())
					<div class="alert alert-danger mb-2" data-aos="fade-up" data-aos-delay="100">
						<ul class="m-0">
							@foreach ($errors->all() as $error)
							<li> {{ $error }} </li>
							@endforeach
						</ul>
					</div>
					@endif

					<form class="mb-5" action="{{ route('feedback.submit') }}" method="POST">
						@csrf

						<div class="form-group row" data-aos="fade-up" data-aos-delay="200">
							<label for="agreement" class="col-md-4 col-form-label">
								Apakah anda setuju dengan materi yang
								disajikan?
							</label>
							<div class="col-md-6 d-flex align-item-center">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agreement" id="agreement1"
										value="y" onchange="showSuggestionBox()"
										{{ old('agreement') ? old('agreement') == 'y' ? 'checked' : '' : 'checked' }}>
									<label class="form-check-label" for="agreement1">Setuju</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="agreement" id="agreement2"
										value="n" onchange="showSuggestionBox()"
										{{ old('agreement') == 'n' ? 'checked' : ''}}>
									<label class="form-check-label" for="agreement2">Tidak Setuju</label>
								</div>
							</div>
						</div>

						<div class="form-group row" data-aos="fade-up" data-aos-delay="400" id="suggestionBox">
							<label for="suggestion" class="col-md-4 col-form-label">
								Kritik dan saran
							</label>
							<div class="col-md-6 d-flex align-item-center">
								<textarea name="suggestion" id="suggestion" rows="3" class="form-control"></textarea>
							</div>
						</div>

						<div class=" form-group row" data-aos="fade-up" data-aos-delay="600"
							data-aos-anchor-placement="top-bottom">
							<div class="col-md-4"></div>
							<div class="col-md-6">
								<button type="submit" class="btn btn-success px-4">Submit</button>
								<button type="reset" class="btn btn-light px-4">Cancel</button>
							</div>
						</div>

					</form>

					{{-- Has submitted feedback END --}}
					@endif

					{{-- Permission Else --}}
					@else

					<div class="text-center d-flex align-items-center justify-content-center" style="height: 50vh">
						<div>
							<h1 class="display-6">Maaf, anda tidak memiliki akses</h1>
							<h5 class="mt-5 mt-md-3 text-muted">
								Silahkan hubungi System Administrator
							</h5>
						</div>
					</div>
					{{-- Permission End --}}
					@endif
				</div>
			</div>
		</div>

	</div>
</div>
@endsection

{{-- @push('addon-scripts')
<script>
	$(document).ready(() => {
		$('#agreement1').prop('checked');
		showSuggestionBox();
	});

	const showSuggestionBox = () => {
	 	const agree = $('#agreement1').is(':checked');
		const suggestionBox = $('#suggestionBox');

		agree ? suggestionBox.hide() :suggestionBox.show();
 }
</script>
@endpush --}}