@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3 d-none d-md-block">
      <div class="card" data-aos="fade-up" data-aos-delay="300">
        <div class="card-body">
          <x-sidebar-menu type="panel" activeMenu="materials" />
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card full-height" data-aos="zoom-in">
        <div class="my-3 m-md-4">
          <div class="container">
            <h3>Materi</h3>
            <hr>
            <x-alert />

            <a href="{{ route('materials.create') }}" class="btn btn-primary mb-3">Tambah Baru</a>

            <div class="table-responsive">
              <table class="table table-hover scroll-horizontal-vertical w-100" id="resultTable">
                <thead>
                  <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Sequence</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection

@push('addon-scripts')
<script>
  const  dataTable = $('#resultTable').DataTable({
			processing: true,
			serverSide: true,
			ordering: true,
			ajax: {
				url: '{!! url()->current() !!}'
			},
			columns: [
				{	data: 'title', name: 'title' },
				{	data: 'sequence', name: 'sequence' },
				{	data: 'is_active', name: 'is_active' },
				{	
					data: 'action', 
					name: 'action',
					orderable: false,
					searchable: false,
					width: '15%',
          className: 'text-center'
				},
			],
      order: [
        [1, 'asc'],
        [0, 'asc'],
      ]
	})
</script>
@endpush