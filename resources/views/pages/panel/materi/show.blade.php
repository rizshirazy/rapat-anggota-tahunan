@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3 d-none d-md-block">
      <div class="card" data-aos="fade-up" data-aos-delay="300">
        <div class="card-body">
          <x-sidebar-menu type="panel" activeMenu="materials" />
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card full-height" data-aos="zoom-in">
        <div class="my-3 m-md-4">
          <div class="container">
            <h3>Preview Materi</h3>
            <hr>

            <div class="mb-4">
              <h4>{{ $data->title }}</h4>

              <div class="my-4">
                {!! $data->body !!}
              </div>

              @if ($data->document_path)
              <div id="adobe-dc-view" style="height: 90vh"></div>
              <script src="https://documentservices.adobe.com/view-sdk/viewer.js"></script>
              <script type="text/javascript">
                document.addEventListener("adobe_dc_view_sdk.ready", function()
                  {
                    var adobeDCView = new AdobeDC.View({clientId: "{{ config('services.adobe.key') }}", divId: "adobe-dc-view"});
                      adobeDCView.previewFile(
                      {
                        content: {location: {url: "{{ Storage::url($data->document_path) }}"}},
                        metaData: {fileName: "{{ $data->document_origin_name }}"}
                      },{
                        defaultViewMode: "FIT_WIDTH", 
                        showAnnotationTools: false, 
                        showLeftHandPanel: false,
                        dockPageControls: false, 
                        showDownloadPDF: false, 
                        showPrintPDF: false
                      });
                  });
              </script>
              @endif
            </div>


            <div class="d-flex justify-content-between">
              <div>
                <form action="{{ route('materials.destroy', $data->id) }}" method="POST">
                  @csrf @method('DELETE')
                  <button type="submit" class="btn btn-danger px-3">Delete</button>
                </form>
              </div>

              <div>
                <a href="{{ route('materials.edit', $data->id) }}" class="btn btn-primary px-4 mr-1">Edit</a>
                <a href="{{ route('materials.index') }}" class="btn btn-light px-4">Back</a>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection