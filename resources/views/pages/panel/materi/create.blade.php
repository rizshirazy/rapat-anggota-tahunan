@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3 d-none d-md-block">
      <div class="card" data-aos="fade-up" data-aos-delay="300">
        <div class="card-body">
          <x-sidebar-menu type="panel" activeMenu="materials" />
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card full-height" data-aos="zoom-in">
        <div class="my-3 m-md-4">
          <div class="container">
            <h3>Create Materi</h3>
            <hr>

            @if ($errors->any())
            <div class="alert alert-danger">
              <ul class="m-0">
                @foreach ($errors->all() as $error)
                <li> {{ $error }} </li>
                @endforeach
              </ul>
            </div>
            @endif

            <form action="{{ route('materials.store') }}" method="POST" enctype="multipart/form-data">

              @csrf

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ old('title') }}" autofocus
                      required>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="document">Document</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="document" class="custom-file-input" id="document" accept=".pdf"
                          required>
                        <label class="custom-file-label" for="document">Choose file</label>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="body">Body</label>
                    <textarea name="body" id="editor" class="form-control">{{ old('body') }}</textarea>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="sequence">Sequence</label>
                    <input type="number" class="form-control" id="sequence" name="sequence"
                      value="{{ old('body') ?? "99" }}" required>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="is_active">Active</label>
                    <select name="is_active" id="is_active" class="form-control">
                      <option value="n">Inactive</option>
                      <option value="y">Active</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row my-3">
                <div class="col text-right">
                  <button type="submit" class="btn btn-success px-5">Save</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection

@push('addon-scripts')
<script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
<script>
  CKEDITOR.replace('editor');
</script>
@endpush