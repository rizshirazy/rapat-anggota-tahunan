@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3 d-none d-md-block">
      <div class="card" data-aos="fade-up" data-aos-delay="300">
        <div class="card-body">
          <x-sidebar-menu type="panel" activeMenu="feedback" />
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card full-height" data-aos="zoom-in">
        <div class="my-3 m-md-4">
          <div class="container">
            <h3>Feedback</h3>
            <hr>

            <form action="{{ route('feedback.export') }}" method="post">
              @csrf
              <button type="submit" class="btn btn-success px-4 mb-3">Export</button>
            </form>

            <div class="table-responsive">
              <table class="table table-hover scroll-horizontal-vertical w-100" id="resultTable">
                <thead>
                  <tr>
                    <th scope="col">Anggota</th>
                    <th scope="col">Pendapat</th>
                    <th scope="col">Kritik dan Saran</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection

@push('addon-scripts')
<script>
  const  dataTable = $('#resultTable').DataTable({
			processing: true,
			serverSide: true,
			ordering: false,
			ajax: {
				url: '{!! url()->current() !!}'
			},
			columns: [
				{	data: 'user.name', name: 'user.name' },
				{	data: 'agreement', name: 'agreement' },
				{	data: 'suggestion', name: 'suggestion' },
			],
	})
</script>
@endpush