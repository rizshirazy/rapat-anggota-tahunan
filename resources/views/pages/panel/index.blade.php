@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 d-none d-md-block">
			<div class="card" data-aos="fade-up" data-aos-delay="300">
				<div class="card-body">
					<x-sidebar-menu type="panel" activeMenu="dashboard" />
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card full-height" data-aos="zoom-in">
				<div class="my-3 m-4">
					<h3>Dashboard</h3>

					<div class="row">
						<div class="col-md-6">
							<div class="border rounded p-4 mt-3">
								<h4 class="mb-3">Kehadiran Anggota</h4>
								<div class="progress">
									<div class="progress-bar" role="progressbar" style="width: {{ $users_present_percentage ?? 0}}%;"
										aria-valuenow="{{ $users_present_percentage ?? 0}}" aria-valuemin="0" aria-valuemax="100">
										{{ $users_present_percentage ?? 0}}%</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="border rounded p-4 mt-3">
								<h4 class="mb-3">Pendapat Anggota</h4>
								<div class="progress">
									<div class="progress-bar bg-danger" role="progressbar"
										style="width: {{ $feedback_disagree_percentage ?? 0}}%"
										aria-valuenow="{{ $feedback_disagree_percentage ?? 0}}" aria-valuemin="0" aria-valuemax="100"
										data-toggle="tooltip" title="Tidak setuju {{ $feedback_disagree ?? '' }}">
										{{ $feedback_disagree_percentage ?? 0}}%</div>
									<div class="progress-bar bg-success " role="progressbar"
										style="width: {{ $feedback_agree_percentage ?? 0}}%"
										aria-valuenow="{{ $feedback_agree_percentage ?? 0}}" aria-valuemin="0" aria-valuemax="100"
										data-toggle="tooltip" title="Setuju {{ $feedback_agree ?? ''}}">
										{{ $feedback_agree_percentage ?? 0}}%
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="border rounded p-4 mt-3">
								<table class="table table-borderless">
									<tr>
										<th>Jumlah Anggota</th>
										<td>{{ $users_all }}</td>
									</tr>
									<tr>
										<th>Hadir</th>
										<td>{{ $users_present }}</td>
									</tr>
									<tr>
										<th>Tidak Hadir</th>
										<td>{{ $users_absence }}</td>
									</tr>
								</table>
							</div>
						</div>

						<div class="col-md-6">
							<div class="border rounded p-4 mt-3">
								<table class="table table-borderless">
									<tr>
										<th>Setuju</th>
										<td>{{ $feedback_agree }}</td>
									</tr>
									<tr>
										<th>Tidak Setuju</th>
										<td>{{ $feedback_disagree }}</td>
									</tr>
									<tr>
										<th>Belum Memilih</th>
										<td>{{ $have_not_decided }}</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
</div>
@endsection

@push('addon-script')
<script>
	$(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endpush