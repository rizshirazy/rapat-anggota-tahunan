@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3 d-none d-md-block">
      <div class="card" data-aos="fade-up" data-aos-delay="300">
        <div class="card-body">
          <x-sidebar-menu type="panel" activeMenu="users" />
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card full-height" data-aos="zoom-in">
        <div class="my-3 m-md-4">
          <div class="container">
            <h3>Update User</h3>
            <hr>

            @if ($errors->any())
            <div class="alert alert-danger">
              <ul class="m-0">
                @foreach ($errors->all() as $error)
                <li> {{ $error }} </li>
                @endforeach
              </ul>
            </div>
            @endif

            <form action="{{ route('users.update', $data->id) }}" method="POST" enctype="multipart/form-data">
              @method('PUT')
              @csrf

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ $data->username }}"
                           readonly>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $data->name }}" readonly>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $data->email }}"
                           required>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Role</label>

                    @forelse ($roles as $role)
                    <div class="form-check mb-2">
                      <input class="form-check-input" @if ($data->hasRole($role->name)) checked @endif
                      id="role-{{ $role->id }}"
                      type="checkbox"
                      value="{{ $role->name }}"
                      name="roles[]">
                      <label class="form-check-label" for="role-{{ $role->id }}">
                        {{ Str::upper($role->name) }}
                      </label>
                    </div>
                    @empty
                    <p>Not roles available.</p>
                    @endforelse

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Permission</label>

                    @forelse ($permissions as $permission)
                    <div class="form-check mb-2">
                      <input class="form-check-input" @if ($data->hasPermissionTo($permission->name)) checked @endif
                      id="permission-{{ $permission->id }}"
                      type="checkbox"
                      value="{{ $permission->name }}"
                      name="permissions[]">
                      <label class="form-check-label" for="permission-{{ $permission->id }}">
                        {{ Str::upper($permission->name) }}
                      </label>
                    </div>
                    @empty
                    <p>Not permissions available.</p>
                    @endforelse
                  </div>
                </div>
              </div>

              <div class="d-flex justify-content-between my-3">
                <div>
                  <button type="button" class="btn btn-danger px-4"
                          onclick="event.preventDefault();document.getElementById('reset-user-form').submit();">
                    Reset Password
                  </button>
                </div>
                <div>
                  <button type="submit" class="btn btn-success px-4 mr-1">Save</button>
                  <a href="{{ route('users') }}" class="btn btn-light px-4">Cancel</a>
                </div>
              </div>
            </form>

            <form action="{{ route('users.reset', $data->id) }}" method="POST" id="reset-user-form" class="d-none">
              @csrf
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection