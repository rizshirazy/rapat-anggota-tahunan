@extends('layouts.app')

@section('content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-3 d-none d-md-block">
      <div class="card" data-aos="fade-up" data-aos-delay="300">
        <div class="card-body">
          <x-sidebar-menu type="panel" activeMenu="users" />
        </div>
      </div>
    </div>

    <div class="col-md-9">
      <div class="card full-height" data-aos="zoom-in">
        <div class="my-3 m-md-4">
          <div class="container">
            <h3>Users</h3>
            <hr>
            <x-alert />

            <div class="row">
              <div class="col-md-6">
                <form action="{{ url('/panel/users/upload-users-file') }}" method="POST" enctype="multipart/form-data"
                  class="mb-3">
                  @csrf
                  <div class="input-group">
                    <div class="custom-file">
                      <input type="file" name="file" class="custom-file-input" id="file" accept=".csv">
                      <label class="custom-file-label" for="file">Choose file</label>
                    </div>
                    <div class="input-group-append">
                      <button class="btn btn-outline-success" type="submit" id="uploadBtn">Upload</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>


            <div class="table-responsive">
              <table class="table table-hover scroll-horizontal-vertical w-100" id="resultTable">
                <thead>
                  <tr>
                    <th scope="col">Username</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection

@push('addon-scripts')
<script>
  const  dataTable = $('#resultTable').DataTable({
			processing: true,
			serverSide: true,
			ordering: true,
			ajax: {
				url: '{!! url()->current() !!}'
			},
			columns: [
				{	data: 'username', name: 'username' },
				{	data: 'name', name: 'name' },
				{	data: 'email', name: 'email' },
				{	
					data: 'action', 
					name: 'action',
					orderable: false,
					searchable: false,
					width: '15%'
				},
			]
	})
</script>
@endpush