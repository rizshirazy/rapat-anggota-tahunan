@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 d-none d-md-block">
			<div class="card" data-aos="fade-up" data-aos-delay="300">
				<div class="card-body">
					<x-sidebar-menu activeMenu="" />
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card full-height"
				 style="background: #f4f4f4 url('https://source.unsplash.com/QY2z-GHpipM') no-repeat center; background-size: cover; position: relative;"
				 data-aos="zoom-in">
				<div>
				</div>
				<div class="card-body d-flex align-items-center justify-content-center text-white"
					 style="background-color: rgba(1, 1, 1, 0.4); position: absolute; top: 0; left: 0; width: 100%;height: 100%; border-radius: 0.25rem !important">
					<div class="w-100">
						<h1 class="text-center">Selamat Datang</h1>
						<h3 class="text-center">pada Rapat Anggota Tahunan</h3>
						<img src="{{asset('storage/images/logo.png')}}" alt="logo koperasi simpan pinjam indosurya"
							 class="mx-auto d-block py-4">
						<p class="text-center text-uppercase">tahun buku 2020 - 2022</p>
						<div class="d-flex justify-content-center d-md-none">
							<a href="{{ route('profile') }}" class="btn btn-outline-light">Mulai</a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	@endsection