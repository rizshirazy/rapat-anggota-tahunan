@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 d-none d-md-block">
			<div class="card" data-aos="fade-up" data-aos-delay="300">
				<div class="card-body">
					<x-sidebar-menu activeMenu="profile" />
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card full-height px-md-3 py-md-5" data-aos="zoom-in">
				<h2 class="text-center d-none d-md-block mb-4">Profil Koperasi</h2>
				<div class="row">
					<div class="col-md-6">
						<img src="{{ asset('/storage/images/grha-surya-landscape.jpg')}}" alt="grha-surya"
							class="img-fluid rounded m-md-4">
					</div>
					<div class="col-md-6">
						<div class="m-4">
							<p> Koperasi Simpan Pinjam Indosurya Cipta merupakan Koperasi Simpan Pinjam yang sudah
								mendapatkan ijin dari Kementrian Koperasi dan Usaha Kecil dan Menengah Republik Indonesia sejak tanggal
								27 September 2012 dengan nomor badan hukum 430/BH/XII.1/-1.829.31/XI/2012 dan Nomor Induk Koperasi (NIK)
								3173080020001 selanjutnya disebut “Indosurya Simpan Pinjam”. </p>
							<p> Indosurya Simpan Pinjam, berusaha untuk membantu para anggotanya dalam mengembangkan
								usahanya pada saat mereka memerlukan modal kerja. Indosurya Simpan Pinjam menghimpun dana dari anggota
								dan calon anggota yang kemudian menyalurkan kembali dana tersebut kepada anggota dan calon anggota
								dalam bentuk
								pinjaman. </p>
							<p> Saat ini pelaksanaan Indosurya Simpan Pinjam dilakukan oleh pengurus dan pengelola yang
								sangat berpengalaman dalam dunia keuangan, terutama perbankan. Selain itu keseluruhan manajemen
								Indosurya Simpan Pinjam sangat menjunjung tinggi integritas dalam mengembangkan usahanya dan dalam
								mengelola dana.
							</p>
						</div>
					</div>
				</div>
				<div class="row d-md-none">
					<div class="col-md-12">
						<div class="d-flex justify-content-center my-3">
							<a href="{{ route('material') }}" class="btn btn-danger px-4">Lanjut</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection