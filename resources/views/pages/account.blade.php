@extends('layouts.auth')

@section('content')

<style>
	.glass {
		background: rgba(0, 0, 0, 0.36);
		border-radius: 16px;
		box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
		backdrop-filter: blur(10px);
		-webkit-backdrop-filter: blur(10px);
		color: #f4f4f4
	}
</style>
<div style="height: 100vh; weight: 100vh; background: #f4f4f4 url('https://source.unsplash.com/4pAFsbZYN_I') no-repeat center; background-size: cover"
	 class="d-flex align-items-center justify-content-center">
	<div class="card m-3 p-3 p-md-5 glass">
		<div class="card-body">
			<h1 class="pb-4 text-center">Cek Nomor Anggota</h1>

			@if (session('success'))
			<div class="alert alert-light text-center"> {{ session('success') }}</div>
			<div class="mt-4 d-flex justify-content-center">
				<a href="{{ route('login') }}" class="btn btn-outline-light px-4">Login</a>
			</div>
			@else

			@if (session('error'))
			<div class="alert alert-danger"> {{ session('error') }}</div>
			@endif
			<form action="{{ route('check-account.check') }}" method="POST">
				@csrf

				<div class="form-group row">
					<label for="account_number" class="col-md-12">{{ __('Nomor Rekening') }}</label>
					<div class="col-md-12">
						<input id="account_number" type="account_number"
							   class="form-control @error('account_number') is-invalid @enderror" name="account_number"
							   value="{{ old('account_number') }}" required autocomplete="account_number" autofocus>

						@error('account_number')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
				</div>

				<div class="form-group row">
					<label for="account_name" class="col-md-12">{{ __('Nama Pemilik Rekening') }}</label>
					<div class="col-md-12">
						<input id="account_name" type="account_name"
							   class="form-control @error('account_name') is-invalid @enderror" name="account_name"
							   value="{{ old('account_name') }}" required autocomplete="account_name">

						@error('account_name')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
					</div>
				</div>

				<button type="submit" class="btn btn-block btn-danger mt-4 py-2">Kirim</button>
				<button type="button" class="btn btn-block btn-link" style="color:white;text-decoration:none"
						data-toggle="modal" data-target="#helpModal">
					Bantuan
				</button>
			</form>
			@endif

		</div>
	</div>
</div>

<div class="modal fade" id="helpModal" tabindex="-1" aria-labelledby="helpModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" id="helpModal">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title font-weight-bold" id="helpModalLabel">Petunjuk Pengisian</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<ul>
					<li>Masukkan <strong>nomor rekening</strong> dan <strong>nama pemilik rekening</strong> sesuai
						dengan bukti
						kepemilikan rekening yang anda miliki. (Buku tabungan / sertifikat simpanan berjangka)</li>
					<li>Pastikan penulisan <strong>nama pemilik rekening</strong> sudah sesuai dengan yang tercetak pada
						bukti
						kepemilikan rekening, termasuk spasi dan tanda baca.
					</li>
					<!--<li>Untuk sertifikat simpanan berjangka dengan kode awalan C atau CN, <strong>nomor-->
					<!--		rekening</strong>-->
					<!--	menggunakan kode simpanan berjangka. <small><em>Contoh: CN 02543</em></small>-->
					<!--</li>-->
					<li>Jika mengalami kendala saat pencarian nomor anggota, dapat ditanyakan melalui WhatsApp
						di nomor <strong>085716970930</strong> pada jam kerja (Senin sd Jumat, pukul 09.00
						sd 17.00)
					</li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endsection