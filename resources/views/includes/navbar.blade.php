<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" data-aos="fade-down">
	<div class="container-fluid px-md-4">
		<a class="navbar-brand" href="{{ url('/') }}">
			<img src="{{asset('storage/images/logo.png')}}" alt="logo koperasi simpan pinjam indosurya" style="width: 150px">
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- Left Side Of Navbar -->
			<ul class="navbar-nav mr-auto">

			</ul>

			<!-- Right Side Of Navbar -->
			<ul class="navbar-nav ml-auto">
				<!-- Authentication Links -->

				<li class="nav-item active">
					<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">{{ Auth::user()->name }}</a>
				</li>

				@if (Auth::user()->hasRole('anggota'))
				<li class="nav-item d-block d-md-none">
					<a class="nav-link" href="{{ url('/profile-koperasi') }}">
						Profil Koperasi
					</a>
				</li>
				<li class="nav-item d-block d-md-none">
					<a class="nav-link" href="{{ url('/materi-rapat') }}">
						Materi Rapat Anggota
					</a>
				</li>
				<li class="nav-item d-block d-md-none">
					<a class="nav-link" href="{{ url('/tanggapan-rapat') }}">
						Pendapat Anggota
					</a>
				</li>
				@endif

				@if (Auth::user()->hasRole('admin'))
				<li class="nav-item d-block d-md-none">
					<a class="nav-link disabled" href="#" aria-disabled="true">
						<hr>
					</a>
				</li>
				<li class="nav-item d-block d-md-none">
					<a class="nav-link" href="{{ url('/panel') }}">
						Dashboard
					</a>
				</li>
				@if (Auth::user()->hasPermissionTo('manage user'))
				<li class="nav-item d-block d-md-none">
					<a class="nav-link" href="{{ url('/panel/users') }}">
						Users
					</a>
				</li>
				@endif
				@if (Auth::user()->hasPermissionTo('manage materi'))
				<li class="nav-item d-block d-md-none">
					<a class="nav-link" href="{{ url('/panel/materials') }}">
						Master Materi
					</a>
				</li>
				@endif
				<li class="nav-item d-block d-md-none">
					<a class="nav-link" href="{{ url('/panel/feedback') }}">
						Feedback
					</a>
				</li>

				@endif
				<li class="nav-item">
					<a class="btn btn-danger btn-block" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
						{{ __('Logout') }}
					</a>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
						@csrf
					</form>
				</li>
			</ul>
		</div>
	</div>
</nav>