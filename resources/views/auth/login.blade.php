@extends('layouts.auth')

@section('content')
<div style="position: relative">
	<div style="position: absolute;left: 0; z-index: 100;"
		 class="pt-3 px-2 px-md-4 w-100 d-flex justify-content-between align-items-center">
		<img src="{{ asset('/storage/images/logo.png')}}" alt="logo-indosurya" style="width:230px;height:58px">
		<img src="{{ asset('/storage/images/logo_koperasi.png')}}" alt="logo-koperasi" style="width:75px;height:75px">
	</div>
	<div class="row justify-content-center align-items-center" style="height: 100vh;width: 100vw; margin:0">
		<div class="col-md-6 d-none d-md-block"
			 style="height: 100vh; background: #f4f4f4 url('https://source.unsplash.com/76ExMNYZomo') no-repeat center; background-size: cover">
		</div>

		<div class="col-md-6">
			<div class="">
				<div class="card-body p-4 p-md-5 mt-5 mt-md-1">

					<h5 class="text-center text-uppercase font-weight-bold my-3 my-md-4">rapat anggota</h5>

					<form method="POST" action="{{ route('login') }}" class="py-3 py-md-4">
						@csrf

						<div class="form-group row">
							<label for="username" class="col-md-8 offset-md-2">{{ __('Nomor Anggota') }}</label>
							<div class="col-md-8 offset-md-2">
								<input id="username" type="username" class="form-control @error('username') is-invalid @enderror"
									   name="username" placeholder="Nomor Anggota (00001234)" value="{{ old('username') }}" required autocomplete="username" autofocus>

								@error('username')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>

						<div class="form-group row">
							<label for="password" class="col-md-8 offset-md-2">{{ __('Password') }}</label>

							<div class="col-md-8 offset-md-2">
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
									   name="password" placeholder="88888*** (3 digit terakhir nomor anggota)" required autocomplete="current-password">

								@error('password')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>

						<div class="form-group row mb-2 mt-4">
							<div class="col-md-8 offset-md-2">
								<button type="submit" class="btn btn-danger btn-block">
									{{ __('Login') }}
								</button>
							</div>

						</div>

						<div class="mt-4 d-flex justify-content-center">
							<a href="{{ route('check-account.index') }}" class="btn btn-link">Cek nomor anggota
								disini.</a>
						</div>
					</form>
				</div>
			</div>
		</div>


	</div>
</div>
@endsection