@extends('layouts.app')

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-3 d-none d-md-block">
			<div class="card" data-aos="fade-up" data-aos-delay="300">
				<div class="card-body">
					<x-sidebar-menu type="home" activeMenu="password" />
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card full-height" data-aos="zoom-in">
				<div class="my-3 m-md-4">
					<div class="container">
						<h3>Change Password</h3>
						<hr>
						<x-alert />

						@if ($errors->any())
						<div class="alert alert-danger">
							<ul class="m-0">
								@foreach ($errors->all() as $error)
								<li> {{ $error }} </li>
								@endforeach
							</ul>
						</div>
						@endif

						<form action="{{ route('password.store') }}" method="POST" id="changePassword">
							@method('PUT')
							@csrf

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="current_password">Current Password</label>
										<input type="password" class="form-control" id="current_password" name="current_password" autofocus>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="new_password">New Password</label>
										<input type="password" class="form-control" id="new_password" name="new_password">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="new_password_confirm">New Password Confirm</label>
										<input type="password" class="form-control" id="new_password_confirm" name="new_password_confirm">
									</div>
								</div>
							</div>

							<div class="d-flex justify-content-between my-3">
								<div>
									<button type="submit" class="btn btn-success px-4 mr-1">Save</button>
									<button type="button" onclick="resetForm()" class="btn btn-light px-4">Reset</button>
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>
@endsection

@push('addon-scripts')
<script>
	const resetForm = () => {
	$('#changePassword').trigger('reset');
	}
</script>
@endpush