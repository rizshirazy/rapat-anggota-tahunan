@component('mail::message')
Kepada Yth.
<h3 style="line-height:1em">{{$user->name}}</h3>
<p style="margin-top:2em">
    Kami mengundang Anda untuk berpartisipasi pada Rapat Anggota Tahunan Koperasi Simpan Pinjam Indosurya Cipta tahun
    buku 2019. Rapat Anggota ini akan dilakukan secara daring melalui website mulai tanggal 17 November 2020 sampai
    dengan 30 November 2020.
</p>
<p>Berikut informasi login anda</p>

<table width="100%" align="center">
    <tr>
        <th style="text-align:right">username</th>
        <td style="padding-left:2px;padding-right:3px">:</td>
        <td>{{$user->username}}</td>
    </tr>
    <tr>
        <th style="text-align:right">password</th>
        <td style="padding-left:2px;padding-right:3px">:</td>
        <td>{{$password}}</td>
    </tr>
</table>

@component('mail::button', ['url' => 'http://google.com', 'color' => 'red'])
Login
@endcomponent

<p>
    Kami berharap partisipasi dari Anda pada Rapat Anggota ini.
</p>

@component('mail::subcopy')
Salam,

<h5>Koperasi Simpan Pinjam Indosurya Cipta</h5>
@endcomponent
@endcomponent