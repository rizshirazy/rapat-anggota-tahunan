@props(['type' => 'home', 'panel'])

@if ($type == 'home')
<ul class="list-group list-group-flush">
    <a href="{{ url('/profile-koperasi') }}"
       class="list-group-item list-group-item-action {{ $isActive('profile') ? 'active' : '' }}">
        Profil Koperasi
    </a>
    <a href="{{ url('/materi-rapat') }}"
       class="list-group-item list-group-item-action {{ $isActive('material') ? 'active' : '' }}">
        Materi Rapat Anggota
    </a>
    <a href="{{ url('/tanggapan-rapat') }}"
       class="list-group-item list-group-item-action {{ $isActive('feedback') ? 'active' : '' }}">
        Pendapat Anggota
    </a>
    <a href="{{ route('password.index') }}"
       class="list-group-item list-group-item-action {{ $isActive('password') ? 'active' : '' }}">
        Change Password
    </a>
    @if (Auth::user()->hasPermissionTo('show panel'))
    <a href="{{ url('/panel') }}" class="list-group-item list-group-item-action">
        Admin Panel
    </a>
    @endif
</ul>
@endif

@if ($type == 'panel')
<ul class="list-group list-group-flush">
    <a href="{{ url('/panel') }}"
       class="list-group-item list-group-item-action {{ $isActive('dashboard') ? 'active' : '' }}">
        Dashboard
    </a>

    @if (Auth::user()->hasPermissionTo('manage user'))
    <a href="{{ url('/panel/users') }}"
       class="list-group-item list-group-item-action {{ $isActive('users') ? 'active' : '' }}">
        Users
    </a>
    @endif

    @if (Auth::user()->hasPermissionTo('manage materi'))
    <a href="{{ url('/panel/materials') }}"
       class="list-group-item list-group-item-action {{ $isActive('materials') ? 'active' : '' }}">
        Materi
    </a>
    @endif

    <a href="{{ url('/panel/feedback') }}"
       class="list-group-item list-group-item-action {{ $isActive('feedback') ? 'active' : '' }}">
        Feedback
    </a>
</ul>
@endif