@if (Session::has('message'))
    @php
        list($type, $message) = explode('|', Session::get('message'));
    @endphp
    <div class="alert alert-{{$type}} alert-dismissible fade show" role="alert">
        {{$message}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <script>
    setTimeout(() => {
        $('.alert').alert('close')
    }, 5000);
    </script>
@endif