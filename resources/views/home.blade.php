@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 d-none d-md-block">
            <div class="card" data-aos="fade-up" data-aos-delay="600">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <a href="#" class="list-group-item list-group-item-action">Profil Koperasi</a>
                        <a href="#" class="list-group-item list-group-item-action">Pengurus dan Pengelola</a>
                        <a href="#" class="list-group-item list-group-item-action">Materi Rapat Anggota</a>
                        <a href="#" class="list-group-item list-group-item-action">Pendapat Anggota</a>
                      </ul>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="card full-height" data-aos="zoom-in" >
                <div class="card-body d-flex align-items-center justify-content-center">
                    <div class="w-100">
                        <div class="welcome-title">Selamat Datang</div>
                        <div class="text-muted my-3 text-center">pada</div>
                        <div class="welcome-subtitle">Rapat Anggota Tahunan</div>
                        <img src="{{asset('storage/images/logo.png')}}" alt="logo koperasi simpan pinjam indosurya" class="mx-auto d-block py-4">
                        <div class="welcome-subtitle">Tahun Buku 2021</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
