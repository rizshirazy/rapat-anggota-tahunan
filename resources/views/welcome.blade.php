@extends('layouts.auth')

@section('content')

<style>
    .glass {
        background: rgba(0, 0, 0, 0.36);
        border-radius: 16px;
        box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
        backdrop-filter: blur(10px);
        -webkit-backdrop-filter: blur(10px);
        color: #f4f4f4
    }
</style>

<div style="height:100vh; background: #f4f4f4 url('https://source.unsplash.com/JkMDud8pWds') no-repeat center; background-size: cover"
     data-aos="zoom-in">
    <div class="card-body d-flex align-items-center justify-content-center" style="height: 92vh;">
        <div class="p-2 p-md-5 rounded glass">
            @if ($before)
            <h1 class="text-center">Selamat Datang</h1>
            <h3 class="text-center">rapat anggota akan dimulai pada</h3>
            <h2 class="text-center py-4" id="demo"></h2>
            <p class="text-center">Anda dapat login pada waktu tersebut</p>
            @endif

            @if ($after)
            <h1 class="text-center">Terima Kasih</h1>
            <h3 class="text-center">telah berpartisipasi pada rapat anggota</h3>
            <img src="{{asset('storage/images/logo.png')}}" alt="logo koperasi simpan pinjam indosurya"
                 class="mx-auto d-block py-4">
            <p class="text-center">Sampai jumpa pada kesempatan berikutnya</p>
            @endif

            @if (!$after && !$before)
            <script>
                window.location.replace("{{ route('home') }}")
            </script>
            @endif

            <div class="w-100 d-flex justify-content-center">
                <a class="btn btn-outline-light text-center" href="{{ route('logout') }}" onclick="event.preventDefault();
																										document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
            </div>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </div>

    @if ($before)
    <script>
        // Set the date we're counting down to
					var countDownDate = new Date("{{ $startDate }}").getTime();
					
					// Update the count down every 1 second
					var x = setInterval(function() {
					
						// Get today's date and time
						var now = new Date().getTime();
					
						// Find the distance between now and the count down date
						var distance = countDownDate - now;
					
						// Time calculations for days, hours, minutes and seconds
						var days = Math.floor(distance / (1000 * 60 * 60 * 24));
						var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
						var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
						var seconds = Math.floor((distance % (1000 * 60)) / 1000);
					
						// Display the result in the element with id="demo"
						document.getElementById("demo").innerHTML = days + "d " + hours + "h "
						+ minutes + "m " + seconds + "s ";
					
						// If the count down is finished, write some text
						if (distance < 0) {
							clearInterval(x);
							document.getElementById("demo").innerHTML = "EXPIRED";
						}
					}, 1000);
    </script>
    @endif

</div>
@endsection