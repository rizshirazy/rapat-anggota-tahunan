<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Rapat Anggota') }}</title>
    <link rel="shortcut icon" type="image/png" href="https://kospinindosurya.com/assets/images/favicon/favicon.png">

    @stack('prepend-styles')
    @include('includes.style')
    @stack('addon-styles')

</head>

<body>
    <div id="app">
        <main class="">
            @yield('content')
        </main>
    </div>

    @stack('prepend-scripts')
    @include('includes.script')
    @stack('addon-scripts')
</body>

</html>