<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\PanelController;
use App\Http\Controllers\UserController;
use App\Models\Param;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::middleware(['auth'])->get('welcome', function () {
    $start = Param::where('code', 'rat_start')->first()->value;
    $end = Param::where('code', 'rat_end')->first()->value;

    $startDate = \Carbon\Carbon::createFromFormat('d-m-Y H:i', $start);
    $endDate = \Carbon\Carbon::createFromFormat('d-m-Y H:i', $end);

    $today = \Carbon\Carbon::today();
    $before = $today->lessThan($startDate);
    $after = $today->greaterThan($endDate);

    return view('welcome', [
        'startDate' => $startDate,
        'before' => $before,
        'after' => $after
    ]);
})->name('welcome');

Route::middleware(['auth'])->group(function () {
    Route::get('change-password', [ChangePasswordController::class, 'index'])->name('password.index');
    Route::put('change-password', [ChangePasswordController::class, 'store'])->name('password.store');

    Route::middleware(['period'])->group(function () {
        Route::get('/', [HomeController::class, 'index'])->name('home');
        Route::get('/profile-koperasi', [HomeController::class, 'profile'])->name('profile');
        Route::get('/pengurus-koperasi', [HomeController::class, 'committee'])->name('committee');
        Route::get('/tanggapan-rapat', [HomeController::class, 'feedback'])->name('feedback');
        Route::post('/tanggapan-rapat', [HomeController::class, 'submit_feedback'])->name('feedback.submit');
        Route::get('/materi-rapat', [HomeController::class, 'material'])->name('material');
    });
});


Route::prefix('panel')->middleware(['auth', 'admin'])->group(function () {
    Route::get('/', [PanelController::class, 'index'])->name('panel');

    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('users');
        Route::get('/{id}/edit', [UserController::class, 'edit'])->name('users.edit');
        Route::put('/{id}', [UserController::class, 'update'])->name('users.update');
        Route::delete('/{id}', [UserController::class, 'destroy'])->name('users.delete');
        Route::post('/all', [UserController::class, 'allUsers']);
        Route::post('/upload-users-file', [UserController::class, 'uploadUsersFile']);
        Route::post('/{user}/reset-password', [UserController::class, 'resetPassword'])->name('users.reset');
    });

    Route::resource('materials', MaterialController::class)->names('materials');

    Route::get('feedback', [FeedbackController::class, 'index'])->name('panel.feedback');
    Route::post('feedback', [FeedbackController::class, 'export'])->name('feedback.export');
});

Route::get('/send-invitation', [UserController::class, 'sendInvitation']);

Route::get('/cek-nomor-anggota', [AccountController::class, 'index'])->name('check-account.index');
Route::post('/cek-nomor-anggota', [AccountController::class, 'check'])->name('check-account.check');
