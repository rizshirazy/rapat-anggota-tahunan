<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'      => 'System Administrator',
            'email'     => 'admin@kospinindosurya.com',
            'password'  => Hash::make('admin'),
            'username'  => '00000000'
        ]);

        $admin->assignRole('admin');
        $admin->givePermissionTo(['manage materi', 'manage user', 'manage profile']);
    }
}
