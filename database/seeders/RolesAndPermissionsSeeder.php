<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $submit_feedback = Permission::create(['name' => 'submit feedback']);
        $role_anggota = Role::create(['name' => 'anggota']);
        $role_anggota->givePermissionTo($submit_feedback);

        $show_panel = Permission::create(['name' => 'show panel']);
        $role_admin = Role::create(['name' => 'admin']);
        $role_admin->givePermissionTo($show_panel);

        Permission::create(['name' => 'manage materi']);
        Permission::create(['name' => 'manage user']);
        Permission::create(['name' => 'manage profile']);
    }
}
