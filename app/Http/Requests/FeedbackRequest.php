<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = User::findOrFail(Auth::id());

        return $user->hasPermissionTo('submit feedback');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'agreement' => 'required|in:y,n',
            'description' => 'nullable|string|max:1000',
            'suggestion' => 'required_if:agreement,n|max:1000',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'agreement.required' => 'Pernyataan setuju harus diisi',
            'description.max' => 'Tanggapan materi tidak lebih dari :max karakter',
            'suggestion.max' => 'Kritik dan saran tidak lebih dari :max karakter',
            'suggestion.required_if' => 'Kritik dan saran harus diisi',
        ];
    }
}
