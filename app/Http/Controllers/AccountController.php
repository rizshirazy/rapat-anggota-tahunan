<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckCustomerRequest;
use App\Models\Account;
use App\Models\Param;
use App\Models\User;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $start = Param::where('code', 'rat_start')->first()->value;
        $end = Param::where('code', 'rat_end')->first()->value;

        $startDate = \Carbon\Carbon::createFromFormat('d-m-Y H:i', $start)->subDays(30);
        $endDate = \Carbon\Carbon::createFromFormat('d-m-Y H:i', $end)->addDays(30);

        $check = \Carbon\Carbon::now()->between($startDate, $endDate);

        // if (!$check) {
        //     abort(404);
        // }

        return view('pages.account');
    }

    public function check(CheckCustomerRequest $request)
    {
        $account_number = $request->get('account_number');
        $account_name = trim(strtoupper($request->get('account_name')));

        $account = Account::where('number', $account_number)->where('name', $account_name)->first();

        if ($account && $account->user()->exists()) {
            return redirect()
                ->route('check-account.index')
                ->with('success', 'Nomor Anggota Anda: ' . $account->user->username);
        }

        return redirect()
            ->route('check-account.index')
            ->with('error', 'Data tidak ditemukan. Silahkan klik tombol bantuan.')
            ->withInput();
    }
}
