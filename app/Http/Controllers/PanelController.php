<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use App\Models\LoginLog;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class PanelController extends Controller
{
    public function index()
    {
        $admin = User::where('username', '00000000')->first();

        $users_all = User::where('id', '!=', $admin->id)->count();
        $users_present = count(LoginLog::select('user_id')
            ->where('user_id', '!=', $admin->id)
            ->groupBy('user_id')
            ->get());
        $users_absence = $users_all - $users_present;

        $feedback_all = Feedback::where('user_id', '!=', $admin->id)->count();
        $feedback_agree = Feedback::where('agreement', 'y')->where('user_id', '!=', $admin->id)->count();
        $feedback_disagree = $feedback_all - $feedback_agree;
        $have_not_decided = $users_present - $feedback_agree - $feedback_disagree;

        $users_present_percentage =
            $users_all == 0 ? 0 : 100 * $users_present / $users_all;
        $feedback_agree_percentage =
            $feedback_all == 0 ? 0 :  100 * $feedback_agree / $users_present;
        $feedback_disagree_percentage =
            $feedback_all == 0 ? 0 : 100 * $feedback_disagree / $users_present;

        return view(
            'pages.panel.index',
            [
                'users_all' => $users_all,
                'users_present' => $users_present,
                'users_absence' => $users_absence,
                'feedback_all' => $feedback_all,
                'feedback_agree' => $feedback_agree,
                'feedback_disagree' => $feedback_disagree,
                'have_not_decided' => $have_not_decided,
                'users_present_percentage' => number_format($users_present_percentage, 2),
                'feedback_agree_percentage' => number_format($feedback_agree_percentage, 2),
                'feedback_disagree_percentage' => number_format($feedback_disagree_percentage, 2),
            ]
        );
    }
}
