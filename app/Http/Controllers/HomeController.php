<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use App\Models\Feedback;
use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('pages.home');
    }

    public function profile()
    {
        return view('pages.profile');
    }

    public function committee()
    {
        return view('pages.committee');
    }

    public function material()
    {
        $materials = Material::where('is_active', 'y')
            ->orderBy('sequence', 'asc')
            ->orderBy('title', 'asc')
            ->get();

        return view('pages.material', [
            'materials' => $materials
        ]);
    }

    public function feedback()
    {
        $feedback = Feedback::where('user_id', Auth::id())->count();

        return view('pages.feedback', [
            'feedback' => $feedback
        ]);
    }

    public function submit_feedback(FeedbackRequest $request)
    {
        $data = $request->all();

        $data['user_id'] = Auth::id();

        $count = Feedback::where('user_id', Auth::id())->count();

        if ($count < 1) {
            Feedback::create($data);
        }

        return redirect()->route('feedback');
    }
}
