<?php

namespace App\Http\Controllers;

use App\Http\Requests\MaterialRequest;
use App\Models\Material;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $query = Material::query();

            return DataTables::of($query)
                ->addColumn('action', function ($item) {
                    return '
                        <a href="' . route('materials.show', $item->id) . '" class="btn btn-secondary">Preview</a>
                        ';
                })
                ->editColumn('is_active', function ($item) {
                    return $item->is_active == 'y' ? 'Active' : 'Inactive';
                })
                ->rawColumns(['action'])
                ->make();
        }

        return view('pages.panel.materi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.panel.materi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialRequest $request)
    {
        $data = $request->all();

        if ($file = $request->file('document')) {
            $data['document_path'] = $file->store('assets/materi', 'public');
            $data['document_origin_name'] = $file->getClientOriginalName();
        }

        Material::create($data);

        return redirect()->route('materials.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $materi = Material::findOrFail($id);

        return view('pages.panel.materi.show', [
            'data' => $materi
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $materi = Material::findOrFail($id);

        return view('pages.panel.materi.edit', [
            'data' => $materi
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MaterialRequest $request, $id)
    {
        $data = $request->all();

        if ($file = $request->file('document')) {
            $data['document_path'] = $file->store('assets/materi', 'public');
            $data['document_origin_name'] = $file->getClientOriginalName();
        }

        $materi = Material::findOrFail($id);
        $materi->update($data);

        return redirect()->route('materials.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $materi = Material::findOrFail($id);
        $materi->delete();

        return redirect()->route('materials.index');
    }
}
