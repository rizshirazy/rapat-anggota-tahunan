<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImportUserRequest;
use App\Http\Requests\UserRequest;
use App\Mail\InvitationEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $query = User::where('username', '!=', '00000000');

            return DataTables::of($query)
                ->editColumn('name', function ($item) {
                    $badge = $item->hasRole('admin') ? ' <span class="badge badge-success">Admin</span>' : '';
                    return $item->name . $badge;
                })
                ->addColumn('action', function ($item) {
                    return '
                    <a href="' . route('users.edit', $item->id) . '" class="btn btn-secondary">Edit</a>
                    ';
                })->rawColumns(['name', 'action'])
                ->make();
        }

        return view('pages.panel.user.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $permissions = Permission::all();

        return view('pages.panel.user.edit', [
            'data' => $user,
            'roles' => $roles,
            'permissions' => $permissions
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $data = $request->all();

        $user = User::findOrFail($id);
        $user->syncRoles($request->get('roles'));
        $user->syncPermissions($request->get('permissions'));
        $user->update($data);

        return redirect()->route('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users');
    }

    public static function insertUser($data)
    {
        $value = User::where('username', $data['username'])->get();
        if ($value->count() == 0) {
            $user = User::create($data);
            $user->assignRole('anggota');
        }
    }

    public function uploadUsersFile(Request $request)
    {
        $file = $request->file('file');

        if (!$file) {
            Session::flash('message', 'danger|No file selected.');
            return redirect()->route('users');
        }

        // File details
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $tempPath = $file->getRealPath();
        $fileSize = $file->getSize();
        $mimeType = $file->getMimeType();

        // Valid File Extensions
        $valid_extendsion = array("csv");
        // 2MB in Bytes
        $maxFileSize = 2097152;

        // Check file extension
        if (in_array(strtolower($extension), $valid_extendsion)) {
            // Check file size
            if ($fileSize <= $maxFileSize) {

                // File upload location
                $location = 'uploads';
                // Upload file
                $file->move($location, $filename);

                // Import CSV to Database
                $filepath = public_path($location . "/" . $filename);
                // Reading file 
                $file = fopen($filepath, "r");

                $importData_arr = array();
                $i = 0;

                while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
                    $num = count($filedata);

                    // Skip first row;
                    if ($i === 0) {
                        $i++;
                        continue;
                    }

                    for ($r = 0; $r < $num; $r++) {
                        $importData_arr[$i][] = $filedata[$r];
                    }
                    $i++;
                }

                fclose($file);

                foreach ($importData_arr as $importData) {

                    $password = $importData[3] ? $importData[3] : '99999999';

                    $insertData = array(
                        "username"  => $importData[0],
                        "name"      => $importData[1],
                        "email"     => $importData[2],
                        // "password"  => Hash::make(Str::random(8)),
                        "password"  => Hash::make($password),
                    );
                    $this->insertUser($insertData);
                }
                // die(var_dump($importData_arr));

                Session::flash('message', 'success|Import Successfull.');
            } else Session::flash('message', 'danger|File too large. File must be less than 2MB.');
        } else Session::flash('message', 'danger|Invalid File Extension.');

        return Redirect::route('users');
    }

    public function sendInvitation()
    {
        $users = User::whereIn('id', [9])->get();
        // $users = User::whereNotIn('id', [2])->get();

        foreach ($users as $user) {
            Mail::to("rizkielshirazy@gmail.com")
                ->queue(new InvitationEmail($user, Str::random(8)));
        }

        return Redirect::route('users')->with('message', 'success|Email sent!');
    }

    public function resetPassword(User $user)
    {
        $password = '88888' . Str::substr($user->username, '5', '3');

        $user->update(['password' =>  Hash::make($password)]);
        $user->save();

        return Redirect::route('users')->with('message', 'success|Reset password for ' . $user->name . ' success!');
    }
}
