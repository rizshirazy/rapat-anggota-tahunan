<?php

namespace App\Http\Middleware;

use App\Models\Param;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RATPeriod
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->hasRole('admin')) {
            return $next($request);
        } else {
            $start = Param::where('code', 'rat_start')->first()->value;
            $end = Param::where('code', 'rat_end')->first()->value;

            $startDate = \Carbon\Carbon::createFromFormat('d-m-Y H:i', $start);
            $endDate = \Carbon\Carbon::createFromFormat('d-m-Y H:i', $end);
            $check = \Carbon\Carbon::now()->between($startDate, $endDate);

            if ($check) {
                return $next($request);
            }
            return redirect('welcome');
        }

        return redirect('welcome');
    }
}
