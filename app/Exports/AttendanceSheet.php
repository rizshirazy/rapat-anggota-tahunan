<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class AttendanceSheet implements
	FromCollection,
	WithTitle,
	WithMapping,
	WithColumnFormatting,
	WithEvents,
	WithHeadings,
	ShouldAutoSize
{
	private $startRow = 1;
	private $rows = 1;
	private $no = 0;

	public function title(): string
	{
		return 'Kehadiran';
	}

	public function headings(): array
	{
		return [
			'No',
			'CIF',
			'Nama',
			'IP Address',
			'Tanggal',
			'Keterangan'
		];
	}

	public function collection()
	{
		return User::where('username', '!=', '00000000')
			->orderBy('username')
			->get();
	}

	public function map($user): array
	{
		++$this->rows;
		++$this->no;

		$attendance = $user->logs->first();

		return [
			$this->no,
			$user->username,
			$user->name,
			$attendance ? $user->logs->first()->ip_address : '',
			$attendance ? Date::dateTimeToExcel($user->logs->first()->created_at) : '',
			$attendance ? 'Hadir' : 'Absen',
		];
	}

	public function columnFormats(): array
	{
		return [
			'E' => NumberFormat::FORMAT_DATE_DDMMYYYY,
		];
	}

	public function registerEvents(): array
	{
		return [
			AfterSheet::class => function (AfterSheet $event) {
				$startCell = "A" . $this->startRow;
				$lastComun = $event->sheet->getDelegate()->getHighestColumn();

				$headers = $startCell . ":" . $lastComun . "1"; // All headers
				$body = $startCell . ":" . $lastComun . $this->rows;
				$all = $startCell . ":" . $lastComun . $this->rows;

				$sheet = $event->sheet->getDelegate();

				$sheet->getStyle($headers)->getFont()->setBold(true);
				$sheet->getStyle($headers)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle($headers)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getStyle($headers)->getFill()->getStartColor()->setARGB('11BBBBBB');

				$sheet->getStyle($all)->getFont()->setSize(10);
				$sheet->getStyle($all)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
			},
		];
	}
}
