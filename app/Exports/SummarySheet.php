<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;

class SummarySheet implements
	WithEvents,
	WithColumnWidths,
	WithTitle
{

	public function title(): string
	{
		return 'Rangkuman';
	}

	public function registerEvents(): array
	{
		return [
			AfterSheet::class => function (AfterSheet $event) {

				$sheet = $event->sheet->getDelegate();

				// Kehadiran Anggota

				$sheet->setCellValue('B2', 'KEHADIRAN');

				$sheet->setCellValue('C3', 'Hadir');
				$sheet->setCellValue('D3', '=COUNTIF(Kehadiran!$F:$F,"hadir")');
				$sheet->setCellValue('E3', '=D3/D5');

				$sheet->setCellValue('C4', 'Absen');
				$sheet->setCellValue('D4', '=COUNTIF(Kehadiran!$F:$F,"absen")');
				$sheet->setCellValue('E4', '=D4/D5');

				$sheet->setCellValue('C5', 'Total');
				$sheet->setCellValue('D5', '=SUM(D3:D4)');

				// Pendapat Anggota

				$sheet->setCellValue('B7', 'PENDAPAT ANGGOTA');

				$sheet->setCellValue('C8', 'Setuju');
				$sheet->setCellValue('D8', '=COUNTIF(\'Pendapat Anggota\'!$D:$D,"setuju")');
				$sheet->setCellValue('E8', '=D8/D10');

				$sheet->setCellValue('C9', 'Tidak Setuju');
				$sheet->setCellValue('D9', '=COUNTIF(\'Pendapat Anggota\'!$D:$D,"tidak setuju")');
				$sheet->setCellValue('E9', '=D9/D10');

				$sheet->setCellValue('C10', 'Total');
				$sheet->setCellValue('D10', '=SUM(D8:D9)');

				// Style
				$sheet->mergeCells("B2:E2");
				$sheet->getStyle("B2")->getFont()->setBold(true);
				$sheet->getStyle("B2")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
				$sheet->getStyle("B2")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getStyle("B2")->getFill()->getStartColor()->setARGB('11BBBBBB');
				$sheet->getStyle("B2:E5")->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sheet->getStyle("B5:D5")->getFont()->setBold(true);

				$sheet->mergeCells("B7:E7");
				$sheet->getStyle("B7")->getFont()->setBold(true);
				$sheet->getStyle("B7")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
				$sheet->getStyle("B7")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getStyle("B7")->getFill()->getStartColor()->setARGB('11BBBBBB');
				$sheet->getStyle("B7:E10")->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
				$sheet->getStyle("B10:D10")->getFont()->setBold(true);

				$sheet->getStyle("B2:D10")->getFont()->setSize(12);

				$sheet->getStyle("D3:D5")->getNumberFormat()->setFormatCode('#,###');
				$sheet->getStyle("D8:D10")->getNumberFormat()->setFormatCode('#,###');
				$sheet->getStyle("E3:E4")->getNumberFormat()->setFormatCode('0.00%');
				$sheet->getStyle("E8:E9")->getNumberFormat()->setFormatCode('0.00%');

				$sheet->getRowDimension("2:10")->setRowHeight(20);
				$sheet->setShowGridlines(false);
			},
		];
	}


	public function columnWidths(): array
	{
		return [
			'B' => 2,
			'C' => 25,
			'D' => 10,
		];
	}
}
