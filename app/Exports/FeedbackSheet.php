<?php

namespace App\Exports;

use App\Models\Feedback;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class FeedbackSheet implements
	FromCollection,
	ShouldAutoSize,
	WithHeadings,
	WithMapping,
	WithColumnFormatting,
	WithEvents,
	WithTitle
{
	private $startRow = 1;
	private $rows = 1;
	private $no = 0;

	public function title(): string
	{
		return 'Pendapat Anggota';
	}

	public function headings(): array
	{
		return [
			'No',
			'CIF',
			'Nama',
			'Pendapat',
			'Kritik dan Saran',
			'Tanggal',
		];
	}

	public function collection()
	{
		return User::where('username', '!=', '00000000')
			->has('logs')
			->orderBy('username')
			->get();
	}

	public function map($user): array
	{
		++$this->rows;
		++$this->no;

		$feedback = $user->feedback;
		$agreement = $feedback && $user->feedback->agreement == 'y' ? 'Setuju' : 'Tidak Setuju';

		return [
			$this->no,
			$user->username,
			$user->name,
			$feedback ? $agreement : 'Setuju',
			$feedback ? $feedback->suggestion : '',
			$feedback ? Date::dateTimeToExcel($feedback->created_at) : '',
		];
	}

	// public function collection()
	// {
	// 	$admin = User::where('username', '00000000')->first();

	// 	return Feedback::with('user')
	// 		->where('user_id', '!=', $admin->id)
	// 		->get();
	// }

	// public function map($data): array
	// {
	// 	++$this->rows;
	// 	++$this->no;

	// 	return [
	// 		$this->no,
	// 		$data->user->username,
	// 		$data->user->name,
	// 		$data->agreement == 'y' ? 'Setuju' : 'Tidak Setuju',
	// 		$data->suggestion,
	// 		Date::dateTimeToExcel($data->created_at),
	// 	];
	// }

	public function columnFormats(): array
	{
		return [
			'F' => NumberFormat::FORMAT_DATE_DDMMYYYY,
		];
	}

	public function registerEvents(): array
	{
		return [
			AfterSheet::class => function (AfterSheet $event) {
				$startCell = "A" . $this->startRow;
				$lastComun = $event->sheet->getDelegate()->getHighestColumn();

				$headers = $startCell . ":" . $lastComun . "1"; // All headers
				$body = $startCell . ":" . $lastComun . $this->rows;
				$all = $startCell . ":" . $lastComun . $this->rows;

				$sheet = $event->sheet->getDelegate();

				$sheet->getStyle($headers)->getFont()->setBold(true);
				$sheet->getStyle($headers)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
				$sheet->getStyle($headers)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
				$sheet->getStyle($headers)->getFill()->getStartColor()->setARGB('11BBBBBB');

				$sheet->getStyle($all)->getFont()->setSize(10);
				$sheet->getStyle($all)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
			},
		];
	}
}
