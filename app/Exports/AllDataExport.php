<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AllDataExport implements FromCollection, WithMultipleSheets
{
    use Exportable;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return User::all();
    }

    public function sheets(): array
    {
        return [
            new SummarySheet(),
            new AttendanceSheet(),
            new FeedbackSheet()
        ];
    }
}
