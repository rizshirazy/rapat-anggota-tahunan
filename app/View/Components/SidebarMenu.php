<?php

namespace App\View\Components;

use Illuminate\View\Component;

class SidebarMenu extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $activeMenu;
    public $type;

    public function __construct($activeMenu, $type = 'home')
    {
        $this->activeMenu = $activeMenu;
        $this->type = $type;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.sidebar-menu');
    }

    public function isActive($menuId)
    {
        return $menuId === $this->activeMenu;
    }
}
