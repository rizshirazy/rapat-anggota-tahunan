<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Rapat Anggota - Koperasi Simpan Pinjam Indosurya</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="author" content="Six Revisions" />
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <style>
      body {
        /* Location of the image */
        background-image: url(pengumuman-RAT-berakhir.jpg);

        /* Image is centered vertically and horizontally at all times */
        background-position: center center;

        /* Image doesn't repeat */
        background-repeat: no-repeat;

        /* Makes the image fixed in the viewport so that it doesn't move when 
     the content height is greater than the image height */
        background-attachment: fixed;

        /* This is what makes the background image rescale based on its container's size */

        /* Pick a solid background color that will be displayed while the background image is loading */
        background-color: #fff;

        /* SHORTHAND CSS NOTATION
   * background: url(background-photo.jpg) center center cover no-repeat fixed;
   */
      }

      /* For mobile devices */
      @media only screen and (max-width: 767px) {
        body {
          /* The file size of this background image is 93% smaller
     * to improve page load speed on mobile internet connections */
          background-image: url(pengumuman-RAT-berakhir.jpg);
          background-size: 100%;
          background-color: #fff;
        }
      }
    </style>
  </head>
  <body></body>
</html>
