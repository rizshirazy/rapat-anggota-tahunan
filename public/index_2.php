<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PENGUMUMAN</title>
    
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
</head>

<body style="padding: 5rem 2.5rem; font-size: 1.1rem; font-family: 'Lato', sans-serif;">
    <h2 style="text-align:center; margin-bottom:50px;">PENGUMUMAN</h2>

    <p>
        Kepada Yth. <br>
        Seluruh Anggota Koperasi Simpan Pinjam Indosurya Cipta <br>
    </p></br>
    <p> Dengan Hormat,
    </p>
    <p>Sehubungan dengan kendala teknis dalam pelaksanaan Rapat Anggota Tahunan yang akan diselenggarakan secara online. Dengan ini kami informasikan kepada seluruh Anggota bahwa Rapat Anggota Tahunan Koperasi Simpan Pinjam Indosurya Cipta Tahun Buku 2020-2021 belum dapat dilaksanakan pada hari ini.
        Untuk tanggal dan waktu pelaksanaan Rapat Anggota Tahunan selanjutnya Koperasi akan menginformasikan kepada Anggota melalui website dan atau melalui link yang telah disampaikan Koperasi kepada Anggota melalui SMS.
    </p>
    <p style="margin-bottom: 50px;">Jakarta, 29 Juli 2022</p>
    <p style="font-weight: bold;">Pengurus</p>
</body>

</html>